### Important
---
- Requires [.NET 8.0 x64 Desktop Runtime](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/runtime-desktop-8.0.8-windows-x64-installer). The binaries in the Releases page are for **Windows** only! I don't do Linux or Mac so I have no way to even test compiling for those platforms.
- Do NOT put this in Program Files. This is a portable application and will not run when launched from Program Files.
 
Detailed feature list is over at the [AceCorp Engineering website](https://accensi.gitlab.io/acecorpengineering/acecorp-launcher.html#features). [Doomworld thread](https://www.doomworld.com/forum/topic/130149-acecorp-launcher/) maintained by UndeadRyker.

### Notes
---
- Hit F1 for help.
- Do not run as admin or drag and dropping won't work. See [this](https://stackoverflow.com/questions/17416765/drag-and-drop-event-can-not-be-fired-when-running-visual-studio-in-administrator) for why.

### Why should I use this over X Y Z?
---
You shouldn't. If you want an everyday launcher, classic ZDL or even DoomRunner will work just fine. ACL was designed around Git-heavy mods like Deathstrider or HDest and their hefty number of addons. There are a couple of QoL features but otherwise this launcher is geared toward mod developers and/or power users who don't mind typing. If the thought of installing Git for Windows or the lack of Babby's First Application UI option clutter scares you to death, close this page, take your anxiety pills, and use something else. There are plenty of options much better suited for the average user. If you're not utilizing the full potential of ACL, then there's no point in using it for anything other than the dark themes.

### Known issues and "issues"
---
- Moving mods using the arrow keys, then pressing one of those keys without Ctrl will set the selection to the topmost item, but only upon pressing said key; using the scroll wheel will not trigger any of this. This is likely a result of the hacky list update implementation. No known fix at this time and I don't know exactly why it happens.
- Adding your first separator at the end of a list with a visible vertical scrollbar would scroll to the right, moving things out of view. To fix it, resize the window or close and reopen the launcher.