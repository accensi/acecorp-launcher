﻿using System.Diagnostics.CodeAnalysis;
using System.Windows.Media;

namespace ACL;

sealed class Theme
{
	internal enum UiSound
	{
		HoverMod,
		HoverContextMenuItem,
		HoverDropDownItem,
		HoverButton,
		HoverLaunchButton,

		ClickMod,
		ClickContextMenuItem,
		ClickDropDownItem,
		ClickButton,
		ClickLaunchButton,

		OpenDropDown,
		OpenContextMenu
	}

	private static object? LastSoundSource;
	private static readonly AclSoundPlayer[] SoundChannels = new AclSoundPlayer[3];

	internal readonly Dictionary<string, SolidColorBrush> Brushes = [];
	internal readonly Dictionary<string, Color> Colors = [];
	internal readonly Dictionary<UiSound, Sound> UiSounds = [];

	internal static void TryPlayUiSound(UiSound type, object? source = null)
	{
		if (!Core.Options.EnableUiSounds || source != null && LastSoundSource == source)
		{
			return;
		}

		Core.SelectedTheme.UiSounds.TryGetValue(type, out Sound? snd);
		if (string.IsNullOrEmpty(snd?.Path))
		{
			return;
		}

		string path = Path.Combine(Core.SelectedTheme!.Location!.DirectoryName!, snd.Path);
		if (!File.Exists(path))
		{
			return;
		}

		LastSoundSource = source;

		Uri uri = new(path);
		AclSoundPlayer? free = SoundChannels.FirstOrDefault(x => !x.IsPlayingSound || x.Player.Source.Equals(uri));
		if (free?.SoundLimiter.ElapsedMilliseconds > 40)
		{
			free.Player.Source = uri;
			free.Player.Volume = snd.Volume;
			free.Player.Play();
			free.IsPlayingSound = true;
			free.SoundLimiter.Restart();
		}
	}

	internal Theme()
	{
		// [Ace] The default theme does not have any sounds. Intentional.
		Location = new FileInfo(Path.Combine("Themes", "Default.xml"));

		foreach (var key in Application.Current.Resources.Keys)
		{
			switch (Application.Current.Resources[key])
			{
				case SolidColorBrush scb:
				{
					Brushes.Add(key.ToString()!, scb);
					break;
				}
				case Color col:
				{
					Colors.Add(key.ToString()!, col);
					break;
				}
			}
		}
	}
	internal Theme(FileInfo path)
	{
		Location = path;
	}

	static Theme()
	{
		for (int i = 0; i < SoundChannels.Length; ++i)
		{
			SoundChannels[i] = new();
		}
	}

	internal FileInfo Location { get; private set; }

	internal XmlDocument DefaultToXmlDoc()
	{
		XmlDocument doc = new();

		XmlElement bigRoot = doc.CreateElement("Theme");
		XmlElement colorRoot = doc.CreateElement("Colors");
		ProcessColorDictionary(Brushes, "B");
		ProcessColorDictionary(Colors, "C");

		bigRoot.AppendChild(colorRoot);
		doc.AppendChild(bigRoot);
		return doc;

		void ProcessColorDictionary<T>(Dictionary<string, T> dict, string typeString)
		{
			foreach (string key in dict.Keys)
			{
				XmlElement elem = doc.CreateElement(key[4..]);
				if (!string.IsNullOrEmpty(typeString))
				{
					elem.SetAttribute("Type", typeString);
				}
				elem.InnerText = "#" + dict[key]?.ToString()?[3..].ToLowerInvariant();
				colorRoot.AppendChild(elem);
			}
		}
	}
	internal void Init()
	{
		XmlDocument doc = new();
		try
		{
			doc.Load(Location.FullName);
		}
		catch (Exception e)
		{
			Core.Log($"Error loading theme \"{ToString()}\": {e.Message}", Core.LogLevel.Critical);
			return;
		}

		XmlNodeList nodes = doc.GetElementsByTagName("Sounds");
		if (nodes.Count > 0)
		{
			foreach (XmlNode node in nodes[0]!)
			{
				if (node is XmlComment)
				{
					continue;
				}

				XmlElement elem = (XmlElement)node;
				if (Enum.TryParse(typeof(UiSound), elem.Name, out object? type))
				{
					// [Ace] Gotta love discards.
					double.TryParse(elem.GetAttribute("Volume"), NumberStyles.Float, CultureInfo.InvariantCulture, out double vol);
					UiSounds[(UiSound)type] = new(elem.InnerText)
					{
						Volume = vol > 0 ? vol : 0.5,
					};
				}
				else
				{
					Core.Log($"Error loading sound for element \"{elem.Name}\" in theme \"{ToString()}\": invalid tag.", Core.LogLevel.Critical);
				}
			}
		}

		nodes = doc.GetElementsByTagName("Colors");
		if (nodes.Count > 0)
		{
			foreach (XmlNode elem in nodes[0]!)
			{
				try
				{
					switch ((elem as XmlElement)?.GetAttribute("Type"))
					{
						case "B":
						{
							Brushes["Col_" + elem.Name] = new((Color)ColorConverter.ConvertFromString(elem.InnerText));
							break;
						}
						case "C":
						{
							Colors["Col_" + elem.Name] = (Color)ColorConverter.ConvertFromString(elem.InnerText);
							break;
						}
					}
				}
				catch
				{
					Core.Log($"Error loading color for element \"{elem.Name}\" in theme \"{ToString()}\": invalid color.", Core.LogLevel.Critical);
				}
			}
		}
	}

	public override string ToString()
	{
		return Location.Name[..Location.Name.LastIndexOf('.')];
	}

	internal sealed class Sound
	{
		internal string Path { get; private set; }
		internal double Volume { get; init; } = 0.5;

		internal Sound(string path)
		{
			Path = path;
		}
	}
}

static partial class Core
{
	[AllowNull]
	private static Theme _SelectedTheme;

	internal static readonly List<Theme> Themes = [];

	internal static Theme SelectedTheme
	{
		get
		{
			return _SelectedTheme;
		}
		set
		{
			_SelectedTheme = value;
			_SelectedTheme.Init();
		}
	}

	// [Ace] Does not create a new list with each call. Only searches for new ones. Theme data is initialized individually upon selecting one.
	internal static void InitThemes()
	{
		DirectoryInfo dirInfo = new("Themes");
		if (!dirInfo.Exists)
		{
			dirInfo.Create();
		}

		FileInfo[] files = dirInfo.GetFiles();
		foreach (FileInfo file in files)
		{
			if (file.Name.Equals("Default.xml", StringComparison.OrdinalIgnoreCase) || !file.Extension.Equals(".xml", StringComparison.InvariantCultureIgnoreCase))
			{
				continue;
			}

			string name = file.Name[..file.Name.LastIndexOf('.')];
			if (!Themes.Exists(x => x.ToString().Equals(name, StringComparison.OrdinalIgnoreCase)))
			{
				Log($"Found new theme \'{name}\'.");
				Themes.Add(new(file));
			}
		}

		if (!Themes.Exists(x => x.ToString().Equals("Default", StringComparison.OrdinalIgnoreCase)))
		{
			string defPath = Path.Combine(dirInfo.FullName, "Default.xml");
			XmlWriterSettings XmlOptions = new()
			{
				OmitXmlDeclaration = true,
				IndentChars = "\t",
				Indent = true
			};

			Theme def = new();
			using (XmlWriter xm = XmlWriter.Create(defPath, XmlOptions))
			{
				def.DefaultToXmlDoc().WriteContentTo(xm);
			}
			Themes.Insert(0, def);
		}
	}
	internal static Theme FindThemeByName(string name)
	{
		Theme? t = Themes.Find(x => x.ToString().Equals(name, StringComparison.OrdinalIgnoreCase));
		if (t == null)
		{
			Log($"Cannot find theme \"{name}\".");
			return Themes[0];
		}
		return t;
	}
}
