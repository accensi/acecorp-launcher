﻿global using System;
global using System.Collections.Generic;
global using System.ComponentModel;
global using System.Diagnostics;
global using System.Globalization;
global using System.IO;
global using System.Linq;
global using System.Text;
global using System.Threading.Tasks;
global using System.Windows;
global using System.Windows.Input;
global using System.Xml;
global using Microsoft.Win32;
global using static System.Environment;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Controls;
using static System.Globalization.CultureInfo;

namespace ACL;

delegate void ModUpdateEvent(object? sender, ModUpdateEventArgs? e);
delegate void ItemUpdateEvent(object? sender, ItemUpdateEventArgs? e);

struct Options // [Ace] These are simple and do not contain any specific parsing logic, unlike WindowResolution for example.
{
	internal string? GitLocation;
	internal string? SladeLocation;
	internal bool SkipPullDialog;
	internal bool AutoselectNewIwads;
	internal bool AutoselectNewEngines;
	internal bool InvertGameLaunchBehavior;
	internal bool CheckForLauncherUpdates;
	internal bool EnableUiSounds;

	internal void SetDefaults() // [Ace] Only really useful if the default is anything other than the, uh... default.
	{
		CheckForLauncherUpdates = true;
		AutoselectNewIwads = true;
		AutoselectNewEngines = true;
		EnableUiSounds = true;
	}

	// [Ace] The copy paste to end all copy pasting.
	// We set defaults both in here and SetDefaults because the latter is called for fresh inis, whereas the GetOption defaults are used
	// if the options cannot be found or parsed. Like if you go into the config and delete the option definition entirely for some reason.
	internal static void GetOption(XmlDocument doc, string name, ref string? option, string? def = null)
	{
		XmlNodeList nodes = doc.GetElementsByTagName(name);
		option = nodes.Count > 0 ? nodes[0]!.InnerText : def;
	}
	internal static void GetOption(XmlDocument doc, string name, ref bool option, bool def = false)
	{
		XmlNodeList nodes = doc.GetElementsByTagName(name);
		if (nodes.Count == 0 || !bool.TryParse(nodes[0]!.InnerText, out option))
		{
			option = def;
		}
	}
	internal static void GetOption(XmlDocument doc, string name, ref int option, int def = 0)
	{
		XmlNodeList nodes = doc.GetElementsByTagName(name);
		if (nodes.Count == 0 || !int.TryParse(nodes[0]!.InnerText, NumberStyles.Integer, InvariantCulture, out option))
		{
			option = def;
		}
	}
	internal static void GetOption(XmlDocument doc, string name, ref double option, double def = 0)
	{
		XmlNodeList nodes = doc.GetElementsByTagName(name);
		if (nodes.Count == 0 || !double.TryParse(nodes[0]!.InnerText, NumberStyles.Float, InvariantCulture, out option))
		{
			option = def;
		}
	}
}
class ModUpdateEventArgs
{
	internal bool ScrollToBottom { get; set; }
}
class ItemUpdateEventArgs
{
	internal IEnumerable<object>? NewList { get; set; }
	internal object? ToSelect { get; set; }
}
static partial class Core
{
	internal enum MoveDirection
	{
		Up,
		Down
	}

	[Flags]
	internal enum AddModPresetFlags
	{
		Select = 1,
		Rename = 2
	}

	internal enum AddResult
	{
		Fail,
		Add,
		Update
	}

	internal enum LogLevel
	{
		None,
		Normal,
		Critical
	}

	[Flags]
	internal enum LogFlags
	{
		NoTimestamp = 1,
		NoSpam = 2 // [Ace] If present, the message will not show up if it's the same as the last one.
	}

	[Flags]
	internal enum CheckUpdateFlags
	{
		Pull = 1,
		LogIfNoUpdates = 2
	}

	internal const string ReleasesUrl = "https://gitlab.com/accensi/acecorp-launcher/-/releases";
	internal const string TagsXmlUrl = "https://gitlab.com/accensi/acecorp-launcher/-/tags?format=atom";

	private static string _ExtraParams = string.Empty;
	private static readonly object LogLock = new();

	private static bool HasGit;

	internal static event ItemUpdateEvent? OnEngineUpdate;
	internal static event ItemUpdateEvent? OnIwadUpdate;
	internal static event ModUpdateEvent? OnModUpdate;
	internal static event ItemUpdateEvent? OnModPresetUpdate;
	internal static event ItemUpdateEvent? OnLaunchPresetUpdate;
	internal static event PropertyChangedEventHandler? OnPropertyChanged;

	internal static readonly List<Engine> Engines = [];
	internal static readonly List<Iwad> Iwads = [];
	internal static readonly List<ModPreset> ModPresets = [];
	internal static readonly List<LaunchPreset> LaunchPresets = [];
	internal static readonly List<CommandLineAlias> Aliases = [];

	internal static Options Options;

	private static readonly List<string> ConsoleLines = [];
	internal static string ConsoleText
	{
		get
		{
			StringBuilder sb = new();
			foreach (string s in ConsoleLines)
			{
				sb.AppendLine(s);
			}
			return sb.ToString();
		}
	}
	internal static int[] WindowResolution = [800, 650];

	internal static LogLevel EnableLogging { get; set; } = LogLevel.None;
	internal static int VersionNumber
	{
		get
		{
			FileVersionInfo info = Process.GetCurrentProcess().MainModule!.FileVersionInfo;
			return int.Parse(info.ProductVersion!.Replace(".", string.Empty));
		}
	}
	internal static string NameAndVersion
	{
		get
		{
			FileVersionInfo info = Process.GetCurrentProcess().MainModule!.FileVersionInfo;
			return info.ProductName + " " + info.ProductVersion;
		}
	}

	[AllowNull]
	internal static ModPreset SelectedModPreset { get; set; }
	internal static Engine? SelectedEngine { get; set; }
	internal static Iwad? SelectedIwad { get; set; }

	internal static string ExtraParams
	{
		get => _ExtraParams;
		set
		{
			_ExtraParams = value;
			OnPropertyChanged?.Invoke(null, new(nameof(ExtraParams)));
		}
	}

	// ------------------------------------------------------------

	static Core()
	{
		InitCommands();
	}

	// ------------------------------------------------------------

	private static FileInfo? GetCustomConfig()
	{
		if (SelectedEngine == null || SelectedModPreset == null)
		{
			return null;
		}

		string configName = $"*{SelectedModPreset.ConfigName}*.ini";

		FileInfo[]? configs = SelectedEngine.Location.Directory?.GetFiles(configName);

		// [Ace] Try searching in user docs. This is only applicable to GZDoom.
		if (configs?.Length == 0)
		{
			DirectoryInfo dir = new(Path.Combine(GetFolderPath(SpecialFolder.MyDocuments), "My Games", "GZDoom"));
			if (dir.Exists)
			{
				configs = dir.GetFiles(configName);
			}
		}

		return configs != null && configs.Length > 0 ? configs[0] : null;
	}
	private static string[]? GetLaunchCommandLine()
	{
		if (SelectedEngine == null)
		{
			return null;
		}

		StringBuilder coreArgs = new();
		if (SelectedIwad != null)
		{
			coreArgs.Append($"-iwad \"{SelectedIwad.Location.FullName}\" ");
		}

		List<Mod> toRun = SelectedModPreset.Mods.FindAll(x => !x.IsSeparator && !x.Disabled);
		if (toRun.Count > 0)
		{
			coreArgs.Append("-file");
			toRun.ForEach(x => coreArgs.Append($" \"{x.Location.FullName}\""));

			//	[Ace] Now do it again for DEHACKED.
			bool addedDehLine = false;
			foreach (Mod mod in toRun)
			{
				// [Ace] This handles both explicit and implicit loading.
				if (string.IsNullOrEmpty(mod.Location.Extension) || mod.Disabled)
				{
					continue;
				}

				string dehFile = mod.Location.FullName.Replace(mod.Location.Extension, ".deh", StringComparison.OrdinalIgnoreCase);
				if (File.Exists(dehFile))
				{
					if (!addedDehLine)
					{
						coreArgs.Append(" -deh");
						addedDehLine = true;
					}
					coreArgs.Append($" \"{dehFile}\"");
				}
			}
		}

		List<string> globals =
		[
			coreArgs.ToString()
		];

		// [Ace] Unfold the aliases.
		string expanded = ExtraParams;
		Aliases.ForEach(x => expanded = expanded.Replace("$" + x.Shorthand, x.FullString, StringComparison.OrdinalIgnoreCase));

		int opening = expanded.IndexOf('<');
		int closing = expanded.IndexOf('>');

		// [Ace] Get all global declarations. Ideally you won't have more than one, but let's account for it anyway.
		while (opening != -1 && closing != -1 && closing - opening > 0)
		{
			string global = expanded[opening..(closing + 1)];
			expanded = expanded.Replace(global, string.Empty).Trim();

			globals.Add(global[1..^1]); // [Ace] Substring removes the parentheses. They're guaranteed to be at the start and end.

			opening = expanded.IndexOf('<');
			closing = expanded.IndexOf('>');
		}

		string[] instances = expanded.Split("&&");

		for (int i = 0; i < instances.Length; ++i)
		{
			instances[i] = string.Join(" ", string.Join(" ", globals), instances[i]);
		}

		return instances;
	}
	internal static bool LaunchGame()
	{
		if (SelectedEngine == null)
		{
			Log("No engine selected.");
			return false;
		}

		if (CheckMissingFiles())
		{
			OnModUpdate?.Invoke(null, null);
			OnIwadUpdate?.Invoke(null, new() { NewList = Iwads, ToSelect = SelectedIwad });
			OnEngineUpdate?.Invoke(null, new() { NewList = Engines, ToSelect = SelectedEngine });
		}

		// [Ace] Using the object's Exists property can return true when the file is, in fact, missing.
		if (!File.Exists(SelectedEngine.Location.FullName))
		{
			Log("Cannot find the engine specified.");
			return false;
		}

		FileInfo? config = GetCustomConfig();
		string[]? args = GetLaunchCommandLine();

		string logStr = $"Launching {SelectedEngine}";
		if (args?.Length > 1)
		{
			logStr += $" (x{args.Length})";
		}
		if (config != null)
		{
			logStr += $"\n[NOTE] Using config \"{config.Name}\"";
		}
		
		for (int i = 0; i < args?.Length; ++i)
		{
			using Process proc = new();
		
			if (config != null)
			{
				args[i] += $" -config {config.Name}";
			}

			proc.StartInfo = new ProcessStartInfo(SelectedEngine.Location.FullName, args[i])
			{
				WorkingDirectory = SelectedEngine.Location.DirectoryName,
			};

			try
			{
				proc.Start();
			}
			catch (Exception ex)
			{
				Log($"Failed to launch game. Reason: {ex.Message}");
				return false;
			}

			if (args?.Length > 1)
			{
				System.Threading.Thread.Sleep(500); // [Ace] I hate Thread.Sleep but proc.WaitForInputIdle doesn't work for all engines. This prevents two instances of, say, GZDoom, from stepping on each other's toes with regards to reading the ini. Without it, the second instance would fail to read the config and launch with default settings. I don't know if this works with all engines.
			}
		}

		Log(logStr);
		return true;
	}
	internal static bool CheckMissingFiles(bool everything = true)
	{
		List<ListItem> all = [];

		if (everything)
		{
			all.AddRange(Engines);
			all.AddRange(Iwads);
		}
		all.AddRange(SelectedModPreset.Mods);

		bool hasInvalid = false;
		foreach (ListItem it in all)
		{
			if (!it.IsValidPath)
			{
				Log("Cannot find " + it.Location.Name + ".", LogLevel.Critical); // [Ace] Was Normal. Missing files is pretty critical. Print that shit.
				hasInvalid = true;
			}
		}
		return hasInvalid;
	}

	// ------------------------------------------------------------

	internal static Engine? AddEngine(string path, bool fromInit, int index = -1)
	{
		if (!Engines.Exists(x => x.Location.FullName == path))
		{
			Engine item = new(path);
			Engines.Insert(index == -1 ? Engines.Count : index, item);
			Log($"Added engine \"{item}\".");
			if (Engines.Count == 1 || !fromInit && Options.AutoselectNewEngines)
			{
				SelectEngine(item);
			}
			OnEngineUpdate?.Invoke(null, new() { NewList = Engines, ToSelect = SelectedEngine });
			SaveConfig();
			return item;
		}
		return null;
	}
	internal static void SelectEngine(Engine? newEngine)
	{
		if (newEngine == null)
		{
			Log("Invalid engine.");
			return;
		}

		SelectedEngine = newEngine;
		Log($"Engine changed to {newEngine}.");
	}
	internal static void RemoveEngine(Engine? item)
	{
		if (item == null)
		{
			return;
		}

		if (item == SelectedEngine)
		{
			SelectedEngine = null;
		}
		if (Engines.Remove(item))
		{
			Log($"Removed engine \"{item}\".");
			if (Engines.Count == 0)
			{
				Log("No engine selected.");
			}
			else
			{
				SelectEngine(Engines[0]);
			}
			OnEngineUpdate?.Invoke(null, new() { NewList = Engines, ToSelect = SelectedEngine });
			SaveConfig();
		}
	}
	internal static void RenameEngine(string newName)
	{
		if (SelectedEngine == null)
		{
			Log("No engine selected.");
			return;
		}

		if (newName == "*")
		{
			newName = null!;
		}

		if (string.IsNullOrEmpty(newName) || string.IsNullOrWhiteSpace(newName))
		{
			string oldName = SelectedEngine.CleanName;
			SelectedEngine.CustomName = "";
			Log($"Restored {oldName}'s name to {SelectedEngine}.", flags: LogFlags.NoTimestamp);
			OnEngineUpdate?.Invoke(null, new() { NewList = Engines });
			return;
		}

		Log($"Engine {SelectedEngine} renamed to {newName}.", flags: LogFlags.NoTimestamp);
		SelectedEngine.CustomName = newName;
		OnEngineUpdate?.Invoke(null, new() { NewList = Engines });
	}

	// ------------------------------------------------------------

	internal static Iwad? AddIwad(string[] paths, bool fromInit, int index = -1)
	{
		Iwad? first = null;
		foreach (string s in paths)
		{
			if (!Iwads.Exists(x => x.Location.FullName == s))
			{
				Iwad item = new(s);
				first ??= item;

				// [Ace] Preserve selection order when adding multiple IWADs.
				Iwads.Insert(index == -1 || paths.Length > 1 ? Iwads.Count : index, item);
				Log($"Added IWAD \"{item}\".");
			}
		}
		if (first != null)
		{
			if (Iwads.Count == 1 || SelectedIwad == null || !fromInit && paths.Length == 1 && Options.AutoselectNewIwads)
			{
				SelectIwad(first);
			}
			OnIwadUpdate?.Invoke(null, new() { NewList = Iwads, ToSelect = SelectedIwad });
			SaveConfig();
		}

		return first;
	}
	internal static void SelectIwad(Iwad? newIwad)
	{
		if (newIwad == null)
		{
			Log("Invalid IWAD.");
			return;
		}

		SelectedIwad = newIwad;
		Log($"IWAD changed to {newIwad}.");
	}
	internal static void RemoveIwad(Iwad? item)
	{
		if (item == null)
		{
			return;
		}
		if (item == SelectedIwad)
		{
			SelectedIwad = null;
		}
		if (Iwads.Remove(item))
		{
			Log($"Removed IWAD \"{item}\".");
			if (Iwads.Count == 0)
			{
				Log("No IWAD selected.");
			}
			else
			{
				SelectIwad(Iwads[0]);
			}
			OnIwadUpdate?.Invoke(null, new() { NewList = Iwads, ToSelect = SelectedIwad });
			SaveConfig();
		}
	}
	internal static void RenameIwad(string? newName)
	{
		if (SelectedIwad == null)
		{
			Log("No IWAD selected.");
			return;
		}

		if (newName == "*")
		{
			newName = null;
		}

		if (string.IsNullOrWhiteSpace(newName))
		{
			string oldName = SelectedIwad.CleanName;
			SelectedIwad.CustomName = "";
			Log($"Restored {oldName}'s name to {SelectedIwad}.", flags: LogFlags.NoTimestamp);
			OnIwadUpdate?.Invoke(null, new() { NewList = Iwads });
			return;
		}

		Log($"IWAD {SelectedIwad} renamed to {newName}.", flags: LogFlags.NoTimestamp);
		SelectedIwad.CustomName = newName;
		OnIwadUpdate?.Invoke(null, new() { NewList = Iwads });
		SaveConfig();
	}

	// ------------------------------------------------------------

	internal static void AddSeparator(ModPreset preset, int index = -1)
	{
		Mod item = new("---");
		if (index > -1)
		{
			preset.Mods.Insert(index, item);
		}
		else
		{
			preset.Mods.Add(item);
		}
		OnModUpdate?.Invoke(null, new() { ScrollToBottom = true });
	}
	internal static void RenameSeparator(ModPreset preset, Mod sep, string? newName)
	{
		if (sep.CustomName != newName)
		{
			if (string.IsNullOrEmpty(newName))
			{
				Log($"Cleared separator name.");
			}
			else if (string.IsNullOrEmpty(sep.CustomName))
			{
				Log($"Renamed separator to {newName}.");
			}
			else
			{
				Log($"Renamed separator from {sep.CustomName} to {newName}.");
			}
		}
		sep.CustomName = newName;
		OnModUpdate?.Invoke(null, new());
	}
	internal static void AddMod(ModPreset preset, IEnumerable<string> paths, bool recursive = false, bool filesOnly = false)
	{
		if (paths == null)
		{
			return;
		}

		int added = 0;
		foreach (string s in paths)
		{
			if (recursive && Directory.Exists(s))
			{
				List<string> innerPaths = [];
				if (!filesOnly)
				{
					innerPaths.AddRange(Directory.GetDirectories(s));
				}
				innerPaths.AddRange(Directory.GetFiles(s));
				AddMod(preset, innerPaths);
				continue;
			}
			if (!preset.Mods.Exists(x => x.Location.FullName == s))
			{
				preset.Mods.Add(new(s));
				added++;
			}
		}

		if (added > 0)
		{
			OnModUpdate?.Invoke(null, new() { ScrollToBottom = true });
		}
	}
	internal static void AddMod(ModPreset preset, IEnumerable<Mod> mods)
	{
		if (mods == null)
		{
			return;
		}

		int added = 0;
		foreach (Mod mi in mods)
		{
			if (mi.IsSeparator || !preset.Mods.Exists(x => x.Location.FullName == mi.Location.FullName))
			{
				preset.Mods.Add(mi);
				added++;
			}
		}

		if (added > 0)
		{
			OnModUpdate?.Invoke(null, new() { ScrollToBottom = true });
		}
	}
	internal static void RemoveMod(ModPreset preset, IEnumerable<Mod> items)
	{
		int removed = 0;
		foreach (Mod mi in items)
		{
			if (preset.Mods.Remove(mi))
			{
				removed++;
			}
		}

		if (removed > 0)
		{
			OnModUpdate?.Invoke(null, null);
		}
	}
	internal static void MoveMod(ModPreset preset, IEnumerable<Mod> items, MoveDirection dir)
	{
		List<Mod> toMove = new(items);
		switch (dir)
		{
			case MoveDirection.Up:
			{
				for (int i = 0; i < preset.Mods.Count; ++i)
				{
					for (int j = 0; j < toMove.Count; ++j)
					{
						if (preset.Mods[i] == toMove[j])
						{
							if (i == 0)
							{
								return;
							}
							Mod swap = preset.Mods[i - 1];
							preset.Mods[i - 1] = toMove[j];
							preset.Mods[i] = swap;
							toMove[j].HasChanged = true;
							break;
						}
					}
				}
				break;
			}
			case MoveDirection.Down:
			{
				for (int i = preset.Mods.Count - 1; i >= 0; --i)
				{
					for (int j = toMove.Count - 1; j >= 0; --j)
					{
						if (preset.Mods[i] == toMove[j])
						{
							if (i == preset.Mods.Count - 1)
							{
								return;
							}
							Mod swap = preset.Mods[i + 1];
							preset.Mods[i + 1] = toMove[j];
							preset.Mods[i] = swap;
							toMove[j].HasChanged = true;
							break;
						}
					}
				}
				break;
			}
		}
		OnModUpdate?.Invoke(null, null);
	}
	internal static void ToggleMod(ModPreset preset, IEnumerable<Mod> items)
	{
		List<Mod> toToggle = new(items);
		toToggle.RemoveAll(x => x.IsSeparator);
		int disabledCount = toToggle.FindAll(x => x.Disabled).Count;
		toToggle.ForEach(x =>
		{
			x.Disabled = disabledCount != toToggle.Count;
			x.HasChanged = true; // [Ace] Keep the selection.
		});
		OnModUpdate?.Invoke(null, null);
	}
	internal static string? GetLoadOrderAsText(ModPreset preset)
	{
		StringBuilder sb = new();
		foreach (Mod mi in preset.Mods)
		{
			if (mi.IsSeparator || mi.Disabled || !mi.IsValidPath)
			{
				continue;
			}
			sb.AppendLine(mi.Location.Name);
		}
		return sb.Length > 0 ? sb.ToString() : null;
	}

	// ------------------------------------------------------------

	internal static (AddResult, ModPreset?) AddModPreset(string name, List<Mod>? list = null, int index = -1, AddModPresetFlags flags = AddModPresetFlags.Select)
	{
		if (string.IsNullOrEmpty(name))
		{
			Log("Cannot make a preset with an empty name.");
			return (AddResult.Fail, null);
		}

		ModPreset? preset = ModPresets.Find(x => x.Name == name);
		if (preset != null)
		{
			Log($"A preset called \"{name}\" already exists.");
			return (AddResult.Fail, preset);
		}

		AddResult result = AddResult.Add;

		if (flags.HasFlag(AddModPresetFlags.Rename) && SelectedModPreset != null)
		{
			Log($"Preset \"{SelectedModPreset.Name}\" renamed to \"{name}\".");
			SelectedModPreset.Name = name;
			result = AddResult.Update;
		}

		if (result == AddResult.Add)
		{
			preset = new(name);
			ModPresets.Insert(index == -1 ? ModPresets.Count : index, preset);
			Log($"Added new mod preset \"{name}\".");

			// [Ace] If no explicit list is defined and there is a currently selected mod preset (this can ONLY be null during config initialization), create a copy.
			if (list == null && SelectedModPreset != null)
			{
				list = SelectedModPreset.Mods;
			}

			if (list != null)
			{
				foreach (Mod mi in list)
				{
					preset.Mods.Add((Mod)mi.ShallowCopy());
				}
			}

			if (flags.HasFlag(AddModPresetFlags.Select))
			{
				SelectedModPreset = preset;
			}
		}

		OnModPresetUpdate?.Invoke(null, new() { NewList = ModPresets, ToSelect = preset });

		if (result == AddResult.Add)
		{
			OnModUpdate?.Invoke(null, null);
		}

		SaveConfig();
		return (result, preset);
	}
	internal static void RemoveModPreset(ModPreset? item)
	{
		if (item == null)
		{
			return;
		}

		if (ModPresets.Count == 1)
		{
			Log("Cannot remove the last preset.");
			return;
		}

		if (ModPresets.Remove(item))
		{
			Log($"Removed mod preset \"{item}\".");
			SelectedModPreset = ModPresets.Count > 0 ? ModPresets[0] : null;
			OnModPresetUpdate?.Invoke(null, new() { NewList = ModPresets });
			OnModUpdate?.Invoke(null, null);
		}
		
		SaveConfig();
	}
	internal static void LoadModPreset(ModPreset? item)
	{
		if (item == null)
		{
			return;
		}

		if (SelectedModPreset == item)
		{
			Log($"\"{item.Name}\" is already selected.");
			return;
		}

		SelectedModPreset = item;
		OnModUpdate?.Invoke(null, null);
		Log($"Loaded mod preset \"{item.Name}\".");

		CheckMissingFiles(false);
	}

	// ------------------------------------------------------------

	internal static AddResult AddLaunchPreset(string name, string? cfg = null, int index = -1)
	{
		if (string.IsNullOrEmpty(name))
		{
			Log("Cannot make a preset with an empty name.");
			return AddResult.Fail;
		}

		if (string.IsNullOrEmpty(cfg) && string.IsNullOrEmpty(ExtraParams))
		{
			Log("Cannot make a preset with no command line arguments.");
			return AddResult.Fail;
		}

		AddResult result = AddResult.Add;
		LaunchPreset? preset = LaunchPresets.Find(x => x.Name == name);
		if (preset == null)
		{
			preset = new(name, cfg ?? ExtraParams);
			LaunchPresets.Insert(index == -1 ? LaunchPresets.Count : index, preset);
			Log($"Added new launch preset \"{name}\".");
		}
		else
		{
			Log($"Updated existing launch preset \"{name}\".");
			result = AddResult.Update;
		}
		preset.Params = cfg ?? ExtraParams;
		if (result == AddResult.Add)
		{
			OnLaunchPresetUpdate?.Invoke(null, new() { NewList = LaunchPresets });
		}

		SaveConfig();
		return result;
	}
	internal static void RemoveLaunchPreset(LaunchPreset? item)
	{
		if (item == null)
		{
			return;
		}

		if (LaunchPresets.Remove(item))
		{
			Log($"Removed launch preset \"{item}\".");
			OnLaunchPresetUpdate?.Invoke(null, new() { NewList = LaunchPresets });
		}
	}
	internal static void LoadLaunchPreset(LaunchPreset? item)
	{
		if (item == null)
		{
			return;
		}
		ExtraParams = item.Params;
		Log($"Loaded launch preset \"{item.Name}\".");
	}

	// ------------------------------------------------------------

	internal static AddResult AddCommandLineAlias(string name, string cmd) // [Ace] These have no timestamp because they always come after a command that has been timestamped, so it's pointless.
	{
		if (name.Contains(' '))
		{
			Log("Aliases cannot have spaces in them.", flags: LogFlags.NoTimestamp);
			return AddResult.Fail;
		}

		if (string.IsNullOrEmpty(name))
		{
			Log("Cannot make an alias with an empty name.", flags: LogFlags.NoTimestamp);
			return AddResult.Fail;
		}

		if (string.IsNullOrEmpty(cmd))
		{
			Log("Cannot make an alias with no command line arguments.", flags: LogFlags.NoTimestamp);
			return AddResult.Fail;
		}

		AddResult result = AddResult.Add;
		CommandLineAlias? alias = Aliases.Find(x => x.Shorthand == name);
		if (alias == null)
		{
			alias = new(name, cmd);
			Aliases.Add(alias);
			Log($"Added new command line alias \"{name}\".", flags: LogFlags.NoTimestamp);
		}
		else
		{
			alias.FullString = cmd;
			Log($"Updated existing command line alias \"{name}\".", flags: LogFlags.NoTimestamp);
			result = AddResult.Update;
		}

		SaveConfig();
		return result;
	}
	internal static void RemoveCommandLineAlias(string name)
	{
		if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
		{
			return;
		}

		if (Aliases.RemoveAll(x => x.Shorthand.Equals(name, StringComparison.OrdinalIgnoreCase)) > 0)
		{
			Log($"Removed command line alias \"{name}\".", flags: LogFlags.NoTimestamp);
		}
		else
		{
			Log($"Cannot find an alias \"{name}\".", flags: LogFlags.NoTimestamp);
		}
		SaveConfig();
	}

	// ------------------------------------------------------------

	internal static void LoadZdlFile(ModPreset preset, string path)
	{
		string[] text = File.ReadAllLines(path);
		List<string> paths = [];
		foreach (string s in text)
		{
			if (!s.StartsWith("file", StringComparison.OrdinalIgnoreCase))
			{
				continue;
			}
			paths.Add(s[(s.LastIndexOf('=') + 1)..]);
		}
		preset.Mods.Clear();
		AddMod(preset, paths);
	}
	internal static async Task CheckForModUpdates(CheckUpdateFlags flags)
	{
		if (!CheckForGit())
		{
			return;
		}

		int gitMods = SelectedModPreset.Mods.FindAll(x => x.IsGitRepository).Count;
		if (gitMods == 0)
		{
			Log("No valid mods for updating.");
			return;
		}

		List<Task> tasks = [];
		Stopwatch st = new();
		st.Start();
		foreach (Mod mi in SelectedModPreset.Mods)
		{
			tasks.Add(Task.Run(() => mi.CheckForUpdates(flags)));
		}

		await Task.WhenAll(tasks);

		st.Stop();
		Log($"Checked all mods.{NewLine}Total time: " + st.Elapsed.ToString());
		OnModUpdate?.Invoke(null, null);
	}
	internal static bool CheckForGit()
	{
		if (!HasGit)
		{
			Log("Git.exe not located. If it is installed, configure the path in Config.xml");
			return false;
		}
		return true;
	}
	private static void TryFindGit()
	{
		// [Ace] Check the specified location first.
		if (File.Exists(Options.GitLocation))
		{
			HasGit = true;
			return;
		}

		Options.GitLocation = Path.Combine(GetFolderPath(SpecialFolder.ProgramFiles), "Git", "bin", "git.exe");

		// [Ace] Fallback.
		if (!File.Exists(Options.GitLocation))
		{
			Options.GitLocation = Path.Combine(GetFolderPath(SpecialFolder.ProgramFilesX86), "Git", "bin", "git.exe");

			// [Ace] Jesus, where the fuck did you install the thing?
			if (!File.Exists(Options.GitLocation))
			{
				try
				{
					string? basePath = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey("SOFTWARE")?.OpenSubKey("GitForWindows")?.GetValue("InstallPath")?.ToString();
					if (!string.IsNullOrEmpty(basePath))
					{
						Options.GitLocation = Path.Combine(basePath, "bin", "git.exe");
					}
				}
				catch
				{
					// [Ace] It's not fatal to not handle the exception. GitLocation will just be null so no big deal;
					Options.GitLocation = null;
					return;
				}
			}
		}

		if (File.Exists(Options.GitLocation))
		{
			HasGit = true;
		}
	}

	// ------------------------------------------------------------

	internal static void Init()
	{
		InitThemes();

		XmlDocument doc = new();
		try
		{
			doc.Load("Config.xml");
		}
		catch (FileNotFoundException ex)
		{
			Log($"{ex.Message.Replace("Could not find file", "Could not find configuration file at")}. Creating a new one.", LogLevel.Critical);
			SaveConfig(true);
		}
		catch (Exception ex)
		{
			Log($"Failed to load configuration file. Error: {ex.Message}{NewLine}Creating a new one.");
			SaveConfig(true);
		}

		Log("Program started.", LogLevel.Critical);

		XmlNodeList nodes;

		// -------------------- OPTIONS --------------------

		nodes = doc.GetElementsByTagName(nameof(WindowResolution));
		if (nodes.Count > 0)
		{
			string[]? split = nodes[0]!.InnerText.Split('x');
			if (split?.Length == 2 && int.TryParse(split[0], NumberStyles.Integer, InvariantCulture, out int x) && int.TryParse(split[1], NumberStyles.Integer, InvariantCulture, out int y))
			{
				WindowResolution[0] = Math.Clamp(x, 0, (int)SystemParameters.PrimaryScreenWidth);
				WindowResolution[1] = Math.Clamp(y, 0, (int)SystemParameters.PrimaryScreenHeight);
			}
		}

		Options.GetOption(doc, nameof(Options.SladeLocation), ref Options.SladeLocation);
		Options.GetOption(doc, nameof(Options.GitLocation), ref Options.GitLocation);
		Options.GetOption(doc, nameof(Options.EnableUiSounds), ref Options.EnableUiSounds, true);
		Options.GetOption(doc, nameof(Options.SkipPullDialog), ref Options.SkipPullDialog);
		Options.GetOption(doc, nameof(Options.InvertGameLaunchBehavior), ref Options.InvertGameLaunchBehavior);
		Options.GetOption(doc, nameof(Options.CheckForLauncherUpdates), ref Options.CheckForLauncherUpdates, true);
		Options.GetOption(doc, nameof(Options.AutoselectNewIwads), ref Options.AutoselectNewIwads, true);
		Options.GetOption(doc, nameof(Options.AutoselectNewEngines), ref Options.AutoselectNewEngines, true);

		TryFindGit();

		// -------------------- THEMES --------------------

		// [Ace] Set to default and let it be overwritten if possible.
		SelectedTheme = Themes[0];

		nodes = doc.GetElementsByTagName(nameof(SelectedTheme));
		if (nodes.Count > 0)
		{
			if (!Themes.Exists(x => x.ToString().Equals(nodes[0]!.InnerText, StringComparison.OrdinalIgnoreCase)))
			{
				Log($"Invalid theme \"{nodes[0]!.InnerText}\". Switching to Default.", LogLevel.Critical);
			}
			else
			{
				SelectedTheme = FindThemeByName(nodes[0]!.InnerText);
			}
		}

		// -------------------- ENGINES --------------------

		nodes = doc.GetElementsByTagName("Engines");
		if (nodes.Count > 0)
		{
			foreach (XmlElement elem in nodes[0]!)
			{
				Engine item = AddEngine(elem.InnerText, true)!;
				if (elem.GetAttribute("LastSelected") == "True")
				{
					SelectedEngine = item;
				}
				string cName = elem.GetAttribute("CustomName");
				if (!string.IsNullOrEmpty(cName))
				{
					item.CustomName = cName;
				}
			}
		}

		// -------------------- IWADS --------------------

		nodes = doc.GetElementsByTagName("IWADs");
		if (nodes.Count > 0)
		{
			foreach (XmlElement elem in nodes[0]!)
			{
				Iwad item = AddIwad([elem.InnerText], true)!;
				if (elem.GetAttribute("LastSelected") == "True")
				{
					SelectedIwad = item;
				}
				string cName = elem.GetAttribute("CustomName");
				if (!string.IsNullOrEmpty(cName))
				{
					item.CustomName = cName;
				}
			}
		}

		// -------------------- MODS --------------------

		nodes = doc.GetElementsByTagName("ModPreset");
		if (nodes.Count == 0)
		{
			AddModPreset("Default", null);
		}
		else
		{
			bool selectedSomething = false;
			foreach (XmlElement rootElem in nodes)
			{
				List<Mod> mods = [];
				foreach (XmlElement elem in rootElem.ChildNodes)
				{
					Mod item = new(elem.InnerText, elem.GetAttribute("Disabled") == "True");
					string cName = elem.GetAttribute("CustomName");
					if (!string.IsNullOrEmpty(cName))
					{
						item.CustomName = cName;
					}
					mods.Add(item);
				}

				bool selected = rootElem.GetAttribute("LastSelected") == "True";
				if (selected)
				{
					selectedSomething = true;
				}

				AddModPreset(rootElem.GetAttribute("Name"), mods, flags: selected ? AddModPresetFlags.Select : 0);
			}

			// [Ace] When upgrading from 1.9.x to 1.10, there would not be anything that has the LastSelected attribute, or some genius could have removed it to see what happens. Either way, never allow SelectedModPreset to be null.
			if (!selectedSomething)
			{
				SelectedModPreset = ModPresets[0];
			}
		}
		
		// -------------------- LAUNCH PARAMS --------------------

		nodes = doc.GetElementsByTagName("LaunchParams");
		if (nodes.Count > 0)
		{
			XmlElement elem = (XmlElement)nodes[0]!;

			ExtraParams = elem.GetAttribute("Last");
			foreach (XmlElement preset in elem.GetElementsByTagName("LaunchPreset"))
			{
				AddLaunchPreset(preset.GetAttribute("Name"), preset.InnerText);
			}
			foreach (XmlElement alias in elem.GetElementsByTagName("Alias"))
			{
				AddCommandLineAlias(alias.GetAttribute("Name"), alias.InnerText);
			}
		}

		CheckMissingFiles();
	}
	internal static void SaveConfig(bool reset = false, bool fromExit = false)
	{
		XmlDocument doc = new();
		XmlElement bigRoot = doc.CreateElement("ACLConfig");

		// -------------------------------------------------------

		if (reset)
		{
			Options.SetDefaults();
		}

		CreateSimpleElement(nameof(WindowResolution), $"{WindowResolution[0].ToString(InvariantCulture)}x{WindowResolution[1].ToString(InvariantCulture)}");
		CreateSimpleElement(nameof(Options.SladeLocation), Options.SladeLocation);
		CreateSimpleElement(nameof(Options.GitLocation), Options.GitLocation);
		CreateSimpleElement(nameof(Options.EnableUiSounds), Options.EnableUiSounds.ToString());
		CreateSimpleElement(nameof(Options.SkipPullDialog), Options.SkipPullDialog.ToString(), "If set to true, will assume you want to always pull and never fetch.");
		CreateSimpleElement(nameof(Options.InvertGameLaunchBehavior), Options.InvertGameLaunchBehavior.ToString(), "If set to true, holding Ctrl will keep the launcher open instead of closing it, but not holding it will close it.");
		CreateSimpleElement(nameof(Options.CheckForLauncherUpdates), Options.CheckForLauncherUpdates.ToString());
		CreateSimpleElement(nameof(Options.AutoselectNewIwads), Options.AutoselectNewIwads.ToString());
		CreateSimpleElement(nameof(Options.AutoselectNewEngines), Options.AutoselectNewEngines.ToString());
		CreateSimpleElement(nameof(SelectedTheme), SelectedTheme != null ? SelectedTheme.ToString() : "Default");

		if (Engines.Count > 0)
		{
			XmlElement root = doc.CreateElement("Engines");
			foreach (Engine e in Engines)
			{
				XmlElement node = doc.CreateElement("Engine");
				if (SelectedEngine == e)
				{
					node.SetAttribute("LastSelected", "True");
				}
				string? cName = e.CustomName;
				if (!string.IsNullOrEmpty(cName))
				{
					node.SetAttribute("CustomName", cName);
				}
				node.InnerText = e.Location.FullName;
				root.AppendChild(node);
			}
			bigRoot.AppendChild(root);
		}

		if (Iwads.Count > 0)
		{
			XmlElement root = doc.CreateElement("IWADs");
			foreach (ListItem i in Iwads)
			{
				XmlElement node = doc.CreateElement("Iwad");
				if (SelectedIwad == i)
				{
					node.SetAttribute("LastSelected", "True");
				}
				if (!string.IsNullOrEmpty(i.CustomName))
				{
					node.SetAttribute("CustomName", i.CustomName);
				}
				node.InnerText = i.Location.FullName;
				root.AppendChild(node);
			}
			bigRoot.AppendChild(root);
		}

		foreach (ModPreset mp in ModPresets)
		{
			XmlElement root = doc.CreateElement("ModPreset");
			root.SetAttribute("Name", mp.Name);

			if (SelectedModPreset == mp)
			{
				root.SetAttribute("LastSelected", "True");
			}

			foreach (Mod mi in mp.Mods)
			{
				XmlElement node = doc.CreateElement("Mod");
				if (mi.Disabled)
				{
					node.SetAttribute("Disabled", "True");
				}
				if (!string.IsNullOrEmpty(mi.CustomName))
				{
					node.SetAttribute("CustomName", mi.CustomName);
				}
				node.InnerText = mi.IsSeparator ? "---" : mi.Location.FullName;
				root.AppendChild(node);
			}

			bigRoot.AppendChild(root);
		}

		// [Ace] Anon block to limit scope.
		{
			XmlElement root = doc.CreateElement("LaunchParams");
			if (!string.IsNullOrEmpty(ExtraParams))
			{
				root.SetAttribute("Last", ExtraParams);
			}

			foreach (LaunchPreset lp in LaunchPresets)
			{
				XmlElement item = doc.CreateElement("LaunchPreset");
				item.SetAttribute("Name", lp.Name);
				item.InnerText = lp.Params;
				root.AppendChild(item);
			}

			if (Aliases.Count > 0)
			{
				XmlElement subroot = doc.CreateElement("Aliases");
				foreach (CommandLineAlias cla in Aliases)
				{
					XmlElement item = doc.CreateElement("Alias");
					item.SetAttribute("Name", cla.Shorthand);
					item.InnerText = cla.FullString;
					subroot.AppendChild(item);
				}
				root.AppendChild(subroot);
			}

			if (!root.IsEmpty)
			{
				bigRoot.AppendChild(root);
			}
		}

		// -------------------------------------------------------

		doc.AppendChild(bigRoot);

		XmlWriterSettings XmlOptions = new()
		{
			OmitXmlDeclaration = true,
			IndentChars = "\t",
			Indent = true,
			CloseOutput = true
		};

		FileInfo file = new("Config.xml");
		if (file.Exists)
		{
			file.Attributes &= ~FileAttributes.ReadOnly;
		}
		using (XmlWriter xm = XmlWriter.Create("Config.xml", XmlOptions))
		{
			doc.WriteContentTo(xm);
		}

		if (fromExit)
		{
			Log("Program exited.", LogLevel.Critical);
			SaveLogFile();
		}

		void CreateSimpleElement(string name, string? text, string? comment = null)
		{
			if (string.IsNullOrEmpty(text))
			{
				return;
			}

			XmlElement root = doc.CreateElement(name);
			root.InnerText = text;
			if (!string.IsNullOrEmpty(comment))
			{
				bigRoot.AppendChild(doc.CreateComment(comment));
			}
			bigRoot.AppendChild(root);
		}
	}
	internal static string? CheckForLauncherUpdates()
	{
		if (!Options.CheckForLauncherUpdates)
		{
			return null;
		}

		Log("Checking for launcher updates...", LogLevel.Critical);
		try
		{
			using XmlReader reader = XmlReader.Create(TagsXmlUrl);
			while (reader.Read())
			{
				if (reader.Name.Contains("title", StringComparison.OrdinalIgnoreCase))
				{
					string text = reader.ReadInnerXml();
					if (text.StartsWith('v'))
					{
						int num = int.Parse(text[1..].Replace(".", string.Empty));

						reader.Dispose();
						if (VersionNumber < num) // [Ace] The latest update is always going to be at the top.
						{
							return text;
						}
					}
				}
			}
		}
		catch
		{
			Log("Failed to check for updates. The launcher requires an online connection to do that.", LogLevel.Critical);
			return null;
		}

		Log("Launcher is up to date.", LogLevel.Critical);
		return null;
	}
	internal static void OpenReleasesPage(string which)
	{
		Process.Start(new ProcessStartInfo()
		{
			UseShellExecute = true,
			FileName = ReleasesUrl + "/" + which
		});
	}

	// ------------------------------------------------------------

	internal static void Log(string output, LogLevel level = LogLevel.Normal, LogFlags flags = 0)
	{
		lock (LogLock)
		{
			if (EnableLogging > LogLevel.None || level == LogLevel.Critical)
			{
				if (!flags.HasFlag(LogFlags.NoSpam) || ConsoleLines.Count == 0 || !ConsoleLines[^1].Contains(output))
				{
					string str = (!flags.HasFlag(LogFlags.NoTimestamp) ? $"[{DateTime.Now:HH:mm:ss}] " : string.Empty) + $"{output}";
					ConsoleLines.Add(str);
					OnPropertyChanged?.Invoke(null, new(nameof(ConsoleText)));
				}
			}
		}
	}
	internal static void ClearConsole()
	{
		ConsoleLines.Clear();
		OnPropertyChanged?.Invoke(null, new(nameof(ConsoleText)));
	}
	internal static void SaveLogFile()
	{
		StringBuilder existing = new();
		try
		{
			string[] lines = File.ReadAllLines("ConsoleLog.txt");
			existing.AppendJoin('\n', lines[..(Math.Min(lines.Length, 3000) - 1)]);
		}
		catch
		{
			Log($"Failed to load ConsoleLog.txt. Log cleared.", LogLevel.Critical);
		}

		StringBuilder full = new();
		full.AppendLine("============================================================");
		full.AppendLine($"[{NameAndVersion} on {DateTime.Now:yyyy-MM-dd}]{NewLine}");
		full.AppendJoin('\n', ConsoleLines);

		FileInfo file = new("ConsoleLog.txt");
		if (file.Exists)
		{
			file.Attributes &= ~FileAttributes.ReadOnly;
		}
		File.WriteAllText("ConsoleLog.txt", full.ToString() + $"{NewLine}{NewLine}" + existing.ToString());
	}
}

class ListItem
{
	internal ListItem(string path)
	{
		Location = new FileInfo(path);
	}
	internal FileInfo Location { get; set; }
	internal string? CustomName { get; set; }
	internal string CleanName
	{
		get
		{
			int dotIndex = Location.Name.LastIndexOf('.');
			return dotIndex > -1 && !MustIncludeExtension ? Location.Name[..dotIndex] : Location.Name;
		}
	}
	protected bool MustIncludeExtension { get; set; }
	internal virtual bool IsValidPath => File.Exists(Location.FullName) || Directory.Exists(Location.FullName);
	internal virtual void OpenLocation()
	{
		if (!IsValidPath)
		{
			return;
		}

		string argStr = Directory.Exists(Location.FullName) ? $"\"{Location.FullName}\"" : $"/select,\"{Location.FullName}\"";
		Process.Start("explorer.exe", argStr);
	}
	public override string ToString()
	{
		if (!IsValidPath)
		{
			return "[???] " + CleanName;
		}

		// [Ace] Explicit check to see if it's a file.
		if (File.Exists(Location.FullName))
		{
			FileVersionInfo file = FileVersionInfo.GetVersionInfo(Location.FullName);
			return !string.IsNullOrEmpty(CustomName) ? CustomName : !string.IsNullOrEmpty(file.FileDescription) ? file.FileDescription : CleanName;
		}
		return CleanName;
	}
	internal ListItem ShallowCopy()
	{
		return (ListItem)MemberwiseClone();
	}
}

// [Ace] These aren't even used for anything special anymore, but let's keep them separate in case they do in the future.
class Engine : ListItem
{
	internal Engine(string path) : base(path) { }
}

class Iwad : ListItem
{
	internal Iwad(string path) : base(path) { }
}

class Mod : ListItem
{
	internal const int SeparatorLength = 60;

	private bool _HasChanged;
	private bool _Disabled;

	internal Mod(string path, bool startDisabled = false) : base(path)
	{
		if (path.Contains("---"))
		{
			IsSeparator = true;
		}
		MustIncludeExtension = true;

		// [Ace] Set the field directly so we don't trigger an update.
		_Disabled = startDisabled;
	}
	internal string? DetailedInfo
	{
		get
		{
			if (IsSeparator)
			{
				return null;
			}

			string str = Location.FullName;
			if (Disabled)
			{
				str += $"{NewLine}[X] This mod has been disabled.";
			}
			if (!IsValidPath)
			{
				str += $"{NewLine}[???] Could not locate mod. Perhaps it was moved or deleted?";
			}

			return str;
		}
	}
	internal bool IsSeparator { get; private set; } // [Ace] This should have been its own class, but changing it now is probably a bad idea.
	internal bool Disabled
	{
		get => _Disabled;
		set
		{
			HasChanged = _Disabled != value;
			_Disabled = value;
		}
	}
	internal override bool IsValidPath => IsSeparator || base.IsValidPath; // [Ace] Separators count for valid paths because they don't really have one.
	internal bool HasChanged
	{
		get
		{
			bool oldVal = _HasChanged;
			_HasChanged = false;
			return oldVal;
		}
		set
		{
			_HasChanged = value;
		}
	}
	internal bool IsGitRepository => Directory.Exists(Path.Combine(Location.FullName, ".git"));
	internal void CheckForUpdates(Core.CheckUpdateFlags flags)
	{
		if (!IsValidPath || IsSeparator || !IsGitRepository)
		{
			return;
		}

		if (!Core.CheckForGit())
		{
			return;
		}

		Core.Log($"Checking {CleanName} for updates...");

		ProcessStartInfo procStartInfo = new(Core.Options.GitLocation!)
		{
			RedirectStandardOutput = true,
			CreateNoWindow = true,
			WorkingDirectory = Location.FullName
		};

		using Process proc = new();
		proc.StartInfo = procStartInfo;

		StringBuilder sb = new();
		proc.OutputDataReceived += (s, e) => sb.AppendLine(e.Data);

		// [Ace] Fetching is (almost) always safe.
		procStartInfo.Arguments = "fetch";
		proc.Start();
		proc.WaitForExit();

		procStartInfo.Arguments = "status";
		proc.Start();
		proc.BeginOutputReadLine();
		proc.WaitForExit();
		proc.CancelOutputRead();

		string result = sb.ToString();
		bool hasUpdates = result.Contains("behind", StringComparison.OrdinalIgnoreCase);
		bool cannotUpdate = result.Contains("not staged", StringComparison.OrdinalIgnoreCase);

		if (hasUpdates)
		{
			if (cannotUpdate)
			{
				Core.Log($"{CleanName} has updates but cannot be updated. Intervention required.");
			}
			else if (flags.HasFlag(Core.CheckUpdateFlags.Pull))
			{
				procStartInfo.Arguments = "pull";
				procStartInfo.RedirectStandardOutput = false; // [Ace] Having this set to true can cause deadlocks.
				proc.Start();
				proc.WaitForExit();
				Core.Log("Updated " + CleanName + ".");
				return;
			}
		}
		else if (flags.HasFlag(Core.CheckUpdateFlags.LogIfNoUpdates))
		{
			Core.Log($"{CleanName} has no updates.");
		}
	}
	internal override void OpenLocation()
	{
		if (!IsSeparator)
		{
			base.OpenLocation();
		}
	}
	internal void OpenWithSlade()
	{
		if (string.IsNullOrEmpty(Core.Options.SladeLocation))
		{
			Core.Log("Slade path not set up. Configure it in Config.xml.");
			return;
		}
		if (!File.Exists(Core.Options.SladeLocation))
		{
			Core.Log("Slade path is invalid. Make sure you entered its path correctly, eg: 'C:/SLADE/slade.exe'");
			return;
		}

		Process.Start(Core.Options.SladeLocation, $"\"{Location.FullName}\"");
	}
	internal void OpenGitRepository()
	{
		if (!Core.CheckForGit())
		{
			return;
		}

		if (!IsGitRepository)
		{
			Core.Log($"{Location.Name} is not a repository.");
			return;
		}

		ProcessStartInfo procStartInfo = new(Core.Options.GitLocation!);

		procStartInfo.RedirectStandardInput = procStartInfo.RedirectStandardOutput = true;
		procStartInfo.CreateNoWindow = true;
		procStartInfo.WorkingDirectory = Location.FullName;

		using Process proc = new();
		proc.StartInfo = procStartInfo;

		StringBuilder sb = new();
		proc.OutputDataReceived += (s, e) => sb.AppendLine(e.Data);

		procStartInfo.Arguments = "config --get remote.origin.url";
		proc.Start();
		proc.BeginOutputReadLine();
		proc.WaitForExit();

		if (sb.ToString().Contains("https", StringComparison.OrdinalIgnoreCase))
		{
			Process.Start(new ProcessStartInfo()
			{
				UseShellExecute = true,
				FileName = sb.ToString()
			});
		}
		else
		{
			Core.Log("Repository does not have a remote location, does not use HTTPS cloning, or some other silent error occurred.");
		}
	}
	public override string ToString()
	{
		if (IsSeparator)
		{
			string full = string.Empty;
			for (int i = 0; i < SeparatorLength; ++i)
			{
				if (i == 20 && !string.IsNullOrEmpty(CustomName))
				{
					full += "▌ " + CustomName + " ▐";
				}
				full += '▬';
			}
			return full[0..(SeparatorLength)];
		}

		string baseString = base.ToString();
		if (Disabled)
		{
			baseString = "[X] " + baseString;
		}
		return baseString;
	}
}

class CommandLineAlias
{
	internal string Shorthand { get; set; }
	internal string FullString { get; set; }

	internal CommandLineAlias(string shortstr, string longstr)
	{
		Shorthand = shortstr;
		FullString = longstr;
	}
}

class ModPreset
{
	internal List<Mod> Mods = [];
	internal string Name { get; set; }
	internal string ConfigName => string.Concat(Name.Where(x => char.IsLetterOrDigit(x)));

	internal ModPreset(string name)
	{
		Name = name;
	}
	public override string ToString() => Name;
}

class LaunchPreset
{
	internal string Name { get; set; }
	internal string Params { get; set; }
	internal LaunchPreset(string name, string cfg)
	{
		Name = name;
		Params = cfg;
	}
	public override string ToString() => Name;
}