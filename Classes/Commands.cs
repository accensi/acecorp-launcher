﻿using System.Reflection;

namespace ACL;

abstract class Command
{
	internal enum CommandError
	{
		InsufficientArgs,
		UnknownCommand,
		UnknownSubcommand,
		UnknownType
	}

	protected static bool FindAndExecute(string[] args, (string, int, Action)[] cmds)
	{
		foreach (var (cmd, argCount, method) in cmds)
		{
			if (cmd.Equals(args[0], StringComparison.OrdinalIgnoreCase))
			{
				// [Ace] Exclude the command itself.
				if (args.Length - 1 < argCount)
				{
					Error(CommandError.InsufficientArgs, args[0]);
					return false;
				}
				method.Invoke();
				return true;
			}
		}
		return false;
	}
	protected static bool FindAndExecute(string[] args, (string, Action)[] cmds)
	{
		foreach (var (cmd, method) in cmds)
		{
			if (cmd.Equals(args[0], StringComparison.OrdinalIgnoreCase))
			{
				method.Invoke();
				return true;
			}
		}
		return false;
	}

	internal abstract string Name { get; }
	internal virtual string Args => string.Empty;
	internal virtual string Example => string.Empty;
	internal abstract string Description { get; }
	internal abstract void Execute(string[] args);

	internal static void Error(CommandError type, string name)
	{
		string? str = null;
		switch (type)
		{
			case CommandError.InsufficientArgs: str = $"Insufficient arguments for \'{name}\'"; break;
			case CommandError.UnknownCommand: str = $"Unknown command \'{name}\'"; break;
			case CommandError.UnknownSubcommand: str = $"Unknown subcommand \'{name}\'"; break;
			case CommandError.UnknownType: str = $"Unknown type \'{name}\'"; break;
		}

		if (str != null)
		{
			Core.Log(str, flags: Core.LogFlags.NoTimestamp);
		}
	}
}
class HelpCommand : Command
{
	internal override string Name => "help";
	internal override string Args => "[cmd]";
	internal override string Example => "help\nhelp rename";
	internal override string Description => "Shows information about the specified command or lists all commands.";
	internal override void Execute(string[] args)
	{
		if (args.Length == 0)
		{
			StringBuilder sb = new();
			sb.AppendLine("Available commands:" + NewLine);
			foreach (Command c in Core.Commands)
			{
				sb.AppendLine($"{c.Name} {c.Args}");
			}
			sb.Append(NewLine + "For additional help such as syntax, description, and examples for each command, type \'help <cmd>\' without quotes, e.g. \'help rename\'");
			Core.Log(sb.ToString(), flags: Core.LogFlags.NoTimestamp);
			return;
		}

		Command? cmd = Core.Commands.FirstOrDefault(x => x.Name.Equals(args[0]));
		if (cmd == null)
		{
			Error(CommandError.UnknownCommand, args[0]);
			return;
		}

		string ar = !string.IsNullOrEmpty(cmd.Args) ? $"{NewLine}Syntax: {cmd.Name} {cmd.Args}{NewLine}" : "";
		string ds = !string.IsNullOrEmpty(cmd.Description) ? $"{NewLine}Description: {cmd.Description}{NewLine}" : "";
		string ex = !string.IsNullOrEmpty(cmd.Example) ? $"{NewLine}Examples: {NewLine}{cmd.Example}{NewLine}" : "";
		Core.Log($"{ar}{ds}{ex}", flags: Core.LogFlags.NoTimestamp);
	}
}
class ClearCommand : Command
{
	internal override string Name => "clear";
	internal override string Description => "Clears the console.";
	internal override void Execute(string[] args)
	{
		Core.ClearConsole();
	}
}
class ListCommand : Command
{
	internal override string Name => "list";
	internal override string Args => "<aliases|configs>";
	internal override string Example => $"list aliases";
	internal override string Description => "Lists defined things.";
	internal override void Execute(string[] args)
	{
		if (args.Length < 1)
		{
			Error(CommandError.InsufficientArgs, Name);
			return;
		}

		(string, Action)[] cmds =
		[
			("aliases", () =>
			{
				StringBuilder sb = new();
				Core.Aliases.ForEach(x => sb.AppendLine($"${x.Shorthand}: {x.FullString}"));
				Core.Log(sb.ToString(), flags: Core.LogFlags.NoTimestamp);
			}),
			("configs", () =>
			{
				StringBuilder sb = new();
				Core.ModPresets.ForEach(x => sb.AppendLine($"{x.Name} => {x.ConfigName}"));
				Core.Log(sb.ToString(), flags: Core.LogFlags.NoTimestamp);
			})
		];
		
		if (!FindAndExecute(args, cmds))
		{
			Error(CommandError.UnknownType, args[0]);
		}
	}
}
class RenameCommand : Command
{
	internal override string Name => "rename";
	internal override string Args => "<engine|iwad> <new name>";
	internal override string Example => $"rename engine ChocDoom\nrename iwad Doom (Wadsmoosh)";
	internal override string Description => "Renames the currently selected engine or iwad. Use * for name to restore to default.";
	internal override void Execute(string[] args)
	{
		if (args.Length < 2)
		{
			Error(CommandError.InsufficientArgs, Name);
			return;
		}

		(string, Action)[] cmds =
		[
			("engine", () => Core.RenameEngine(string.Join(" ", args[1..]))),
			("iwad", () => Core.RenameIwad(string.Join(" ", args[1..])))
		];

		if (!FindAndExecute(args, cmds))
		{
			Error(CommandError.UnknownType, args[0]);
		}
	}
}
class AliasCommand : Command
{
	internal override string Name => "alias";
	internal override string Args => "<add name command|remove name>";
	internal override string Example => "alias add example -skill 4 +map MAP01 +set sv_cheats 1\nalias remove example";
	internal override string Description => "Adds, removes, or lists all command line aliases. Use $aliasname in the command line args. It will get subtituted on game launch.";
	internal override void Execute(string[] args)
	{
		if (args.Length < 1)
		{
			Error(CommandError.InsufficientArgs, Name);
			return;
		}

		(string, int, Action)[] cmds = 
		[
			("add", 2, () => Core.AddCommandLineAlias(args[1], string.Join(" ", args[2..]))),
			("remove", 1, () => Core.RemoveCommandLineAlias(args[1]))
		];

		if (!FindAndExecute(args, cmds))
		{
			Error(CommandError.UnknownSubcommand, args[0]);
		}
	}
}
static partial class Core
{
	internal static readonly List<Command> Commands = [];

	private static void InitCommands()
	{
		foreach (Type t in Assembly.GetExecutingAssembly().GetTypes())
		{
			if (t.IsAssignableTo(typeof(Command)) && !t.IsAbstract)
			{
				Commands.Add((Command)Activator.CreateInstance(t)!);
			}
		}
	}

	internal static (string?, int) Predict(string input)
	{
		input = input.TrimStart();
		if (input.Length == 0)
		{
			return (null, 0);
		}

		StringBuilder full = new();
		Command? cmd = Commands.Find(x => x.Name.StartsWith(input, StringComparison.OrdinalIgnoreCase));
		if (cmd != null)
		{
			full.Append(cmd.Name);
			if (input.Length > cmd.Name.Length)
			{
				full.Append(input[cmd.Name.Length..]);
			}

			int count = 0;
			for (int i = 0; i < Math.Min(input.Length, full.Length); ++i)
			{
				if (input[i] == full[i])
				{
					count++;
				}
			}
			return new(full.ToString(), count);
		}
		return (null, 0);
	}

	internal static void HandleCommand(string input)
	{
		input = input.Trim();
		if (input.Length == 0)
		{
			return;
		}

		string[] split = input.Split(' ', StringSplitOptions.RemoveEmptyEntries);
		Command? cmd = Commands.Find(x => x.Name.Equals(split[0], StringComparison.OrdinalIgnoreCase));
		if (cmd != null)
		{
			Log(input);
			cmd.Execute(split.Length > 1 ? split[1..] : []);
		}
		else
		{
			Log($"Unknown command \'{split[0]}\'.", flags: LogFlags.NoTimestamp);
		}
	}
}
