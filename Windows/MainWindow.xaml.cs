﻿using System.Windows.Controls;

namespace ACL;

internal class AclSoundPlayer
{
	internal readonly MediaElement Player = new();
	internal readonly Stopwatch SoundLimiter = new();

	internal AclSoundPlayer()
	{
		SoundLimiter.Start();
		Player.LoadedBehavior = MediaState.Manual;
		Player.UnloadedBehavior = MediaState.Manual;
		Player.MediaFailed += (s, e) =>
		{
			if (Core.Options.EnableUiSounds) // [Ace] Log only once.
			{
				Core.Log($"Error playing UI sound: {e.ErrorException.Message}. Sounds have been disabled. Re-enable them from the config file.");
			}
			Core.Options.EnableUiSounds = false;
			Player.Stop();
			SoundLimiter.Reset();
		};
		Player.MediaEnded += (s, e) => IsPlayingSound = false;
	}
	internal bool IsPlayingSound { get; set; }
}
public partial class MainWindow : Window
{
	public MainWindow()
	{
		InitializeComponent();
	}

	internal static bool AllowEvents { get; set; }

	protected override void OnInitialized(EventArgs e)
	{
		base.OnInitialized(e);

		Title = Core.NameAndVersion;

		UpdateTheme(Core.SelectedTheme, true);

		Width = Core.WindowResolution[0];
		Height = Core.WindowResolution[1];

		Core.EnableLogging = Core.LogLevel.Normal;

		// [Ace] Do this once in case something logged to the console prior to the call to OnInitialized().
		ConsoleTextBox.Text = Core.ConsoleText;
		ExtraParamsTextBox.Text = Core.ExtraParams;

		// [Ace] Initial update. Wish I could get rid of it.
		// It's here because the event subscriptions haven't happened yet, but initialization has already passed so even if they
		// were subscribed, nothing would have happened. Probably too much of a clusterfuck to fix it. So let's just leave it alone.
		UpdateAll();

		Core.OnEngineUpdate += (s, e) => UpdateComboBox(EngineList, e);
		Core.OnIwadUpdate += (s, e) => UpdateComboBox(IwadList, e);
		Core.OnModUpdate += (s, e) => UpdateModList(e);
		Core.OnModPresetUpdate += (s, e) => UpdateComboBox(ModPresetList, e);
		Core.OnLaunchPresetUpdate += (s, e) => UpdateComboBox(LaunchPresetList, e);
		Core.OnPropertyChanged += (s, e) =>
		{
			switch (e.PropertyName)
			{
				case nameof(Core.ConsoleText):
				{
					Application.Current.Dispatcher.Invoke(() =>
					{
						ConsoleTextBox.Text = Core.ConsoleText;
						ConsoleTextBox.CaretIndex = ConsoleTextBox.Text.Length; // [Ace] This is for when the console is selected. Doesn't do anything if it's not. That's what ScrollToEnd is for.
						ConsoleTextBox.ScrollToEnd();
					});
					break;
				}
				case nameof(Core.ExtraParams):
				{
					// [Ace] This does not recurse infinitely because apparently OnTextChanged doesn't get called if the text does not, in fact, change.
					// In other words, it won't infinitely assign the same string over and over again.
					// It's probably still a bad idea anyway but oh well.
					// EDIT: Well it's not gonna get called anymore anyway because of AllowEvents being disabled before initialization has completed.
					ExtraParamsTextBox.Text = Core.ExtraParams;
					break;
				}
			}
		};

		Task.Run(() =>
		{
			string? ver = Core.CheckForLauncherUpdates();
			if (!string.IsNullOrEmpty(ver))
			{
				switch (MessageBox.Show($"{ver} is available. Go to the downloads page?{NewLine}{NewLine}Choosing No will stop checking for updates on start.", "Update available!", MessageBoxButton.YesNoCancel))
				{
					case MessageBoxResult.Yes: Core.OpenReleasesPage(ver); break;
					case MessageBoxResult.No: Core.Options.CheckForLauncherUpdates = false; break;
				}
			}
		});

		AllowEvents = true;
		LaunchButton.Focus();
	}

	private void UpdateAll()
	{
		UpdateComboBox(EngineList, new() { NewList = Core.Engines, ToSelect = Core.SelectedEngine });
		UpdateComboBox(IwadList, new() { NewList = Core.Iwads, ToSelect = Core.SelectedIwad });
		UpdateComboBox(ModPresetList, new() { NewList = Core.ModPresets, ToSelect = Core.SelectedModPreset });
		UpdateComboBox(LaunchPresetList, new() { NewList = Core.LaunchPresets }, true);
		UpdateComboBox(ThemeList, new() { NewList = Core.Themes }, true);
		if (Core.SelectedTheme != null)
		{
			ThemeList.SelectedItem = Core.SelectedTheme;
		}
		UpdateTheme(Core.SelectedTheme, true);
		UpdateModList();
	}

	private void UpdateModList(ModUpdateEventArgs? args = null)
	{
		Dispatcher.Invoke(() =>
		{
			ModList.ItemsSource = null;
			ModList.ItemsSource = Core.SelectedModPreset.Mods;

			if (args != null && args.ScrollToBottom && ModList.Items.Count > 0)
			{
				ModList.ScrollIntoView(ModList.Items[^1]);
			}

			List<Mod> items = new(Core.SelectedModPreset.Mods);
			for (int i = items.Count - 1; i >= 0; --i)
			{
				if (items[i].HasChanged)
				{
					ModList.SelectedItems.Add(items[i]);
					ModList.ScrollIntoView(items[i]);
				}
			}
		});
	}
	private void UpdateTheme(Theme? newTheme, bool force = false)
	{
		if (newTheme == null || newTheme == Core.SelectedTheme && !force)
		{
			ThemeList.SelectedItem = Core.SelectedTheme;
			return;
		}

		Core.SelectedTheme = newTheme;
		foreach (string key in newTheme.Brushes.Keys)
		{
			if (Application.Current.Resources[key] != null)
			{
				Application.Current.Resources[key] = newTheme.Brushes[key];
			}
			else
			{
				Core.Log($"Error loading color for element \"{key[4..]}\" in theme \"{newTheme}\": element not found.", Core.LogLevel.Critical);
			}
		}
		if (!force)
		{
			Core.Log($"Updated theme to \"{newTheme}\".");
		}
	}
	private void ChangeFontSize(object sender, double incr)
	{
		const double fac = 96.0 / 72.0;
		const double min = 3 * fac;
		const double max = 16 * fac;
		if (sender == ModList)
		{
			ModList.FontSize = Math.Clamp(ModList.FontSize + incr * fac, min, max);
		}
		if (sender == ConsoleTextBox)
		{
			ConsoleTextBox.FontSize = Math.Clamp(ConsoleTextBox.FontSize + incr * fac, min, max);
		}
	}

	private static void LaunchGame()
	{
		if (!Core.LaunchGame())
		{
			return;
		}
		if ((Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) ^ Core.Options.InvertGameLaunchBehavior)
		{
			Application.Current.Shutdown();
		}
	}

	// [Ace] Sometimes this is Click. Sometimes it's MouseUp. Sometimes it's MouseDown.
	// Does it matter here?
	// No.
	private async void OnClick(object sender, RoutedEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		OpenFileDialog ofd = new()
		{
			CheckFileExists = true,
			CheckPathExists = true
		};

		if (sender == LaunchButton)
		{
			Theme.TryPlayUiSound(Theme.UiSound.ClickLaunchButton);
			LaunchGame();
			return;
		}
		else if (sender is Button b && b.IsEnabled)
		{
			Theme.TryPlayUiSound(Theme.UiSound.ClickButton);
		}
		else if (sender is ComboBoxItem cbi && cbi.IsEnabled)
		{
			Theme.TryPlayUiSound(Theme.UiSound.ClickDropDownItem);
		}
		else if (sender is ListBoxItem lbi && lbi.IsEnabled) // [Ace] Else if because ComboBoxItem inherits from ListBoxItem and we use a dedicated sound for the former.
		{
			Theme.TryPlayUiSound(Theme.UiSound.ClickMod);
		}

		// --------------------------------------------------

		if (sender is Button)
		{
			LaunchButton.Focus();
		}

		// --------------------------------------------------

		if (sender == EngineButton)
		{
			if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
			{
				Core.RemoveEngine(EngineList.SelectedItem as Engine);
			}
			else
			{
				ofd.Filter = "Doom engine (*.exe)|*.exe";
				bool? success = ofd.ShowDialog(this);
				if (success == true)
				{
					Core.AddEngine(ofd.FileName, false, 0);
				}
			}
			return;
		}

		// --------------------------------------------------

		if (sender == IwadButton)
		{
			if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
			{
				Core.RemoveIwad(IwadList.SelectedItem as Iwad);
			}
			else
			{
				ofd.Multiselect = true;
				ofd.Filter = "IWAD (*.ipk3, *.pk3, *.wad, *.pk7, *.zip)|*.ipk3;*.pk3;*.wad;*.pk7;*.zip";
				bool? success = ofd.ShowDialog(this);
				if (success == true)
				{
					Core.AddIwad(ofd.FileNames, false, 0);
				}
			}
			return;
		}

		// --------------------------------------------------

		if (sender == ModPresetButton)
		{
			if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
			{
				Core.RemoveModPreset((ModPreset)ModPresetList.SelectedItem);
				ModPresetList.Focus();
			}
			else
			{
				Core.AddModPresetFlags flags = Core.AddModPresetFlags.Select;
				if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
				{
					flags |= Core.AddModPresetFlags.Rename;
				}
				Core.AddModPreset(ModPresetList.Text, index: 0, flags: flags);
			}
			return;
		}

		// --------------------------------------------------

		if (sender == LaunchPresetButton)
		{
			if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift))
			{
				Core.RemoveLaunchPreset(LaunchPresetList.SelectedItem as LaunchPreset);
				LaunchPresetList.Focus();
			}
			else
			{
				Core.AddLaunchPreset(LaunchPresetList.Text, index: 0);
			}
			return;
		}

		// --------------------------------------------------

		if (sender == ModList)
		{
			ModList.Focus();
			return;
		}

		// --------------------------------------------------

		if (sender == DisableModsButton)
		{
			if (ModList.SelectedItems.Count > 0)
			{
				Core.ToggleMod(Core.SelectedModPreset, ModList.SelectedItems.Cast<Mod>());
				ModList.Focus();
			}
			return;
		}

		// --------------------------------------------------

		if (sender == AddSeparatorButton)
		{
			Core.AddSeparator(Core.SelectedModPreset, ModList.SelectedIndex);
			ModList.Focus();
			return;
		}

		// --------------------------------------------------

		if (sender == GitSyncButton)
		{
			MessageBoxResult result = MessageBoxResult.Yes;
			if (!Core.Options.SkipPullDialog)
			{
				result = MessageBox.Show($"Do you want to PULL any updates? Pressing No will only FETCH changes.{NewLine}{NewLine}NOTE: When updating multiple private repositories for the first time, you may get a login window spam. Log in using one of the windows and close the rest. It should work from then onward.", "STAY UP TO DATE OR ELSE", MessageBoxButton.YesNoCancel);
				if (result == MessageBoxResult.Cancel)
				{
					return;
				}
			}

			await Core.CheckForModUpdates(result == MessageBoxResult.Yes ? Core.CheckUpdateFlags.Pull : 0);
			return;
		}

		// --------------------------------------------------

		if (sender == ThemePickerButton)
		{
			if (ThemeList.Visibility == Visibility.Hidden)
			{
				ThemeList.Visibility = Visibility.Visible;
				UpdateComboBox(ThemeList, new() { NewList = Core.Themes }, true);
				if (Core.SelectedTheme != null)
				{
					ThemeList.SelectedItem = Core.SelectedTheme;
				}
			}
			else
			{
				ThemeList.Visibility = Visibility.Hidden;
			}
			return;
		}
	}
	private void OnRightClick(object sender, MouseButtonEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		if (sender is ComboBox cb && cb.SelectedItem is ListItem li)
		{
			li.OpenLocation();
		}
		else if (sender is ListBox lb)
		{
			var lbi = (ListBoxItem)lb.ItemContainerGenerator.ContainerFromItem(lb.SelectedItem);

			bool hovered = lbi?.IsMouseOver == true; // [Ace] This will be false if we call it after opening the context menu.

			ContextMenu cm = (ContextMenu)Resources["ModListContextMenu"];
			cm.PlacementTarget = lb;
			cm.IsOpen = true;
			cm.Tag = null;
			Theme.TryPlayUiSound(Theme.UiSound.OpenContextMenu);

			foreach (object item in cm.Items)
			{
				if (item is MenuItem i)
				{
					if (i.Name == "AddModFile" || i.Name == "AddModFolder")
					{
						i.IsEnabled = true;
					}
					else
					{
						i.IsEnabled = false;
						if (lb.SelectedItem is Mod mi && hovered && !mi.IsSeparator)
						{
							cm.Tag = mi;		
							if (i.Name == "OpenLocation" || i.Name == "OpenWithSLADE")
							{
								i.IsEnabled = true;
							}
							else if (i.Name == "OpenGitRepo" || i.Name == "UpdateMod")
							{
								i.IsEnabled = mi.IsGitRepository;
							}
						}
					}
				}
			}
		}
	}
	private void OnMenuItemClick(object sender, RoutedEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		if (sender is MenuItem mi && mi.Parent is ContextMenu cm)
		{
			if (mi.IsEnabled)
			{
				Theme.TryPlayUiSound(Theme.UiSound.ClickContextMenuItem);
			}

			switch (mi.Name)
			{
				case "AddModFile":
				{
					OpenFileDialog ofd = new()
					{
						CheckFileExists = true,
						CheckPathExists = true,
						Multiselect = true,
						Filter = "Doom file (*.ipk3, *.pk3, *.wad, *.pk7, *.zip)|*.ipk3;*.pk3;*.wad;*.pk7;*.zip"
					};
					bool? success = ofd.ShowDialog(this);
					if (success == true)
					{
						Core.AddMod(Core.SelectedModPreset, ofd.FileNames);
					}
					break;
				}
				case "AddModFolder":
				{
					OpenFolderDialog ofd = new()
					{
						Multiselect = true
					};
					bool? success = ofd.ShowDialog(this);
					if (success == true)
					{
						Core.AddMod(Core.SelectedModPreset, ofd.FolderNames);
					}
					break;
				}
			}

			if (cm.Tag is Mod selItem)
			{
				switch (mi.Name)
				{
					case "OpenLocation":
					{
						selItem.OpenLocation();
						break;
					}
					case "OpenWithSLADE":
					{
						selItem.OpenWithSlade();
						break;
					}
					case "OpenGitRepo":
					{
						selItem.OpenGitRepository();
						break;
					}
					case "UpdateMod":
					{
						Task.Run(() => selItem.CheckForUpdates(Core.CheckUpdateFlags.Pull | Core.CheckUpdateFlags.LogIfNoUpdates)).ContinueWith(x => UpdateModList());
						break;
					}
				}
			}
		}
	}
	private void OnKeyPress(object sender, KeyEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		if (sender == this)
		{
			switch (e.Key)
			{
				case Key.F1:
				{
					new HelpWindow().ShowDialog();
					break;
				}
			}
			return;
		}

		switch (e.Key)
		{
			case Key.Escape:
			{
				// [Ace] For some stupid reason, hitting escape scrolls to top of various controls like combo boxes, list boxes, and so on.
				// This fixes that.
				e.Handled = true;
				break;
			}
			case Key.Enter:
			{
				// [Ace] In some specific cases like minimizing the launcher after launching a game, then maximizing it and pressing Enter,
				// e.Repeat would be set to true and LaunchGame won't even be reached until you double press Enter, hence the lack of !e.IsRepeat check.
				// I suspect this is because focus is transferred to the game window that just opened, which prevents the event from updating properly.
				// Because this ONLY happens with Enter.
				if (sender == LaunchPresetList)
				{
					Core.LoadLaunchPreset(LaunchPresetList.SelectedItem as LaunchPreset);
					Theme.TryPlayUiSound(Theme.UiSound.ClickDropDownItem);
				}
				else if (sender == LauncherCommandsTextBox)
				{
					Core.HandleCommand(LauncherCommandsTextBox.Text);
					Theme.TryPlayUiSound(Theme.UiSound.ClickButton);
					LauncherCommandsTextBox.Clear();
				}
				else if (sender == ModPresetList)
				{
					Core.LoadModPreset(ModPresetList.SelectedItem as ModPreset);
					Theme.TryPlayUiSound(Theme.UiSound.ClickDropDownItem);
				}
				else
				{
					Theme.TryPlayUiSound(Theme.UiSound.ClickLaunchButton); // [Ace] Counts as a click I suppose.
					LaunchGame();
				}
				break;
			}
			case Key.F5:
			{
				if (!e.IsRepeat)
				{
					Core.Log("Refreshed.");

					Core.CheckMissingFiles();
					Core.InitThemes();

					UpdateAll();
				}
				break;
			}
			case Key.Delete:
			{
				if (Keyboard.Modifiers.HasFlag(ModifierKeys.Shift) && sender == ModList && ModList.SelectedItems.Count > 0 && !e.IsRepeat)
				{
					Core.RemoveMod(Core.SelectedModPreset, ModList.SelectedItems.Cast<Mod>());
				}
				break;
			}
			case Key.C:
			{
				if (sender == ModList && Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
				{
					string? text = Core.GetLoadOrderAsText(Core.SelectedModPreset);
					if (!string.IsNullOrEmpty(text))
					{
						Core.Log("Copied load order.");
						Clipboard.SetText(text);
					}
					else
					{
						Core.Log("Nothing to copy!");
					}
				}
				break;
			}
			case Key.D:
			{
				if (!e.IsRepeat && Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
				{
					ModList.UnselectAll();
				}
				break;
			}
			case Key.Tab:
			{
				if (sender == LauncherCommandsTextBox)
				{
					if (LauncherCommandsTextBox.SelectionLength > 0)
					{
						LauncherCommandsTextBox.CaretIndex = LauncherCommandsTextBox.Text.Length;
						e.Handled = true;
					}
				}
				break;
			}
			case Key.Up:
			{
				if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control) && ModList.SelectedItems.Count > 0)
				{
					Core.MoveMod(Core.SelectedModPreset, ModList.SelectedItems.Cast<Mod>(), Core.MoveDirection.Up);
					e.Handled = true;
				}
				break;
			}
			case Key.Down:
			{
				if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control) && ModList.SelectedItems.Count > 0)
				{
					Core.MoveMod(Core.SelectedModPreset, ModList.SelectedItems.Cast<Mod>(), Core.MoveDirection.Down);
					e.Handled = true;
				}
				break;
			}
		}
	}
	private void OnMouseScroll(object sender, MouseWheelEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		if (Keyboard.Modifiers.HasFlag(ModifierKeys.Control))
		{
			bool scrollMove = false;
			if (sender == ModList && ModList.SelectedItems.Count > 0)
			{
				scrollMove = true;
			}

			if (e.Delta > 0)
			{
				if (scrollMove)
				{
					Core.MoveMod(Core.SelectedModPreset, ModList.SelectedItems.Cast<Mod>(), Core.MoveDirection.Up);
				}
				else
				{
					ChangeFontSize(sender, 1);
				}
				e.Handled = true;
			}
			else if (e.Delta < 0)
			{
				if (scrollMove)
				{
					Core.MoveMod(Core.SelectedModPreset, ModList.SelectedItems.Cast<Mod>(), Core.MoveDirection.Down);
				}
				else
				{
					ChangeFontSize(sender, -1);
				}
				e.Handled = true;
			}
		}
	}
	private void OnModDrop(object sender, DragEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		string[] droppedPaths = (string[])e.Data.GetData(DataFormats.FileDrop);
		string? zdlFile = null;
		foreach (string s in droppedPaths)
		{
			if (s.Contains(".zdl", StringComparison.OrdinalIgnoreCase))
			{
				zdlFile = s;
			}
		}

		if (zdlFile != null)
		{
			Core.LoadZdlFile(Core.SelectedModPreset, zdlFile);
		}
		else
		{
			Core.AddMod(Core.SelectedModPreset, droppedPaths, Keyboard.Modifiers.HasFlag(ModifierKeys.Shift), Keyboard.Modifiers.HasFlag(ModifierKeys.Control));
		}
	}
	private void OnDoubleClick(object sender, MouseButtonEventArgs e)
	{
		if (!AllowEvents || e.ChangedButton == MouseButton.Right)
		{
			return;
		}

		if (sender is ListBox lb && lb.SelectedItem != null)
		{
			bool noneHovered = true;
			foreach (object mod in lb.Items)
			{
				var lbi = lb.ItemContainerGenerator.ContainerFromItem(mod) as ListBoxItem;
				if (lbi?.IsMouseOver == true)
				{
					noneHovered = false;
					break;
				}
			}
			if (noneHovered)
			{
				return;
			}

			Mod item = (Mod)lb.SelectedItem;
			if (item.IsSeparator)
			{
				RenameWindow win = new()
				{
					NewName = item.CustomName
				};
				if (win.ShowDialog() == true)
				{
					Core.RenameSeparator(Core.SelectedModPreset, item, win.NewName);
					return;
				}
			}
			else if (lb.SelectedItems.Count > 0)
			{
				Core.ToggleMod(Core.SelectedModPreset, ModList.SelectedItems.Cast<Mod>());
				return;
			}
		}
	}
	private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
	{
		if (e.AddedItems.Count == 0 || !AllowEvents)
		{
			return;
		}

		if (sender == IwadList)
		{
			if (IwadList.IsDropDownOpen)
			{
				Core.SelectIwad(e.AddedItems[0] as Iwad);
				LaunchButton.Focus();
			}
			return;
		}
		if (sender == EngineList)
		{
			if (EngineList.IsDropDownOpen)
			{
				Core.SelectEngine(e.AddedItems[0] as Engine);
				LaunchButton.Focus();
			}
			return;
		}
		if (sender == ModPresetList)
		{
			if (ModPresetList.IsDropDownOpen)
			{
				Core.LoadModPreset(e.AddedItems[0] as ModPreset);
				LaunchButton.Focus();
			}
			return;
		}
		if (sender == LaunchPresetList)
		{
			if (LaunchPresetList.IsDropDownOpen)
			{
				Core.LoadLaunchPreset(e.AddedItems[0] as LaunchPreset);
				LaunchButton.Focus();
			}
			return;
		}
		if (sender == ThemeList)
		{
			UpdateTheme((Theme)ThemeList.SelectedItem);
			return;
		}
	}
	private void OnTextChanged(object sender, TextChangedEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		if (sender == ExtraParamsTextBox)
		{
			Core.ExtraParams = ExtraParamsTextBox.Text;
			return;
		}
		if (sender == LauncherCommandsTextBox)
		{
			foreach (TextChange tc in e.Changes)
			{
				if (tc.AddedLength == 1)
				{
					(string? cmd, int len) = Core.Predict(LauncherCommandsTextBox.Text);
					if (!string.IsNullOrEmpty(cmd))
					{
						LauncherCommandsTextBox.Text = cmd;
						LauncherCommandsTextBox.Select(len, cmd.Length - len);
					}
				}
			}
			return;
		}
	}
	private void OnSizeChanged(object sender, SizeChangedEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		if (sender == this)
		{
			Core.WindowResolution[0] = (int)Width;
			Core.WindowResolution[1] = (int)Height;
			return;
		}
	}
	private void OnMouseEnter(object sender, MouseEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		if (sender is MenuItem mi && mi.Parent is ContextMenu)
		{
			if (mi.IsEnabled)
			{
				Theme.TryPlayUiSound(Theme.UiSound.HoverContextMenuItem);
			}
		}
		if (sender is ListBoxItem lbi && lbi.IsEnabled)
		{
			Theme.TryPlayUiSound(Theme.UiSound.HoverMod);
			lbi.ToolTip = ((Mod)ModList.ItemContainerGenerator.ItemFromContainer(lbi)).DetailedInfo;
			ToolTipService.SetIsEnabled(lbi, true);
		}

		if (sender is Button btn && btn.IsEnabled)
		{
			Theme.TryPlayUiSound(sender == LaunchButton ? Theme.UiSound.HoverLaunchButton : Theme.UiSound.HoverButton);
		}
	}
	private void OnMouseLeave(object sender, MouseEventArgs e)
	{
		if (!AllowEvents)
		{
			return;
		}

		if (sender is ListBoxItem lbi)
		{
			ToolTipService.SetIsEnabled(lbi, false);
		}
	}
	private void OnMouseMove(object sender, MouseEventArgs e)
	{
		if (sender is ComboBoxItem cbi && cbi.IsEnabled)
		{
			Theme.TryPlayUiSound(Theme.UiSound.HoverDropDownItem, cbi);
		}
	}
	private void OnDropDownOpened(object sender, EventArgs e)
	{
		Theme.TryPlayUiSound(Theme.UiSound.OpenDropDown);
	}
	private void OnDropDownClosed(object sender, EventArgs e)
	{
		Theme.TryPlayUiSound(Theme.UiSound.OpenDropDown);
	}

	// [Ace] I fucking hate hacks. Really need to get rid of this and start using INotifyPropertyChanged. Static classes (Core) are kinda shit at times.
	private static void UpdateComboBox(ComboBox box, ItemUpdateEventArgs? args, bool noSelect = false)
	{
		object oldItem = box.SelectedItem;
		box.ItemsSource = null;
		box.ItemsSource = args?.NewList;
		if (!noSelect)
		{
			box.SelectedItem = args?.ToSelect ?? oldItem;
			if (box.SelectedItem == null && box.Items.Count > 0)
			{
				box.SelectedItem = box.Items[0];
			}
		}
	}
}
