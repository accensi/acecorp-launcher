﻿namespace ACL;

public partial class RenameWindow : Window
{
	public RenameWindow()
	{
		InitializeComponent();
	}

	internal string? NewName { get; set; }

	private void Apply()
	{
		NewName = RenameTextbox.Text;
		DialogResult = true;
		Close();
	}

	private void OnClick(object sender, RoutedEventArgs e)
	{
		if (sender == DoneButton)
		{
			Apply();
			return;
		}
	}
	private void OnLoaded(object sender, EventArgs e)
	{
		if (sender == RenameTextbox)
		{
			RenameTextbox.Text = NewName;
			RenameTextbox.Focus();
			RenameTextbox.SelectAll();
			return;
		}
	}
	private void OnKeyPress(object sender, KeyEventArgs e)
	{
		if (e.Key == Key.Enter)
		{
			Apply();
			return;
		}
	}
}
