﻿using System.Runtime.InteropServices;
using System.Threading;

namespace ACL
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		internal enum ShowWindowCommands
		{
			Restore = 9
		};

		[System.Diagnostics.CodeAnalysis.SuppressMessage("CodeQuality", "IDE0052:Remove unread private members", Justification = "It's not referenced but it's still used.")]
		private static Mutex? Mutex = null;
		private static bool NoLaunch;

		

		[DllImport("user32.dll")]
		internal static extern bool ShowWindow(IntPtr hWnd, ShowWindowCommands nCmdShow);
		[DllImport("user32.dll")]
		internal static extern bool SetForegroundWindow(IntPtr hWnd);
		[DllImport("user32.dll")]
		internal static extern bool IsIconic(IntPtr handle);

		public App() : base()
		{
			Dispatcher.UnhandledException += (s, e) =>
			{
				Core.Log(e.Exception.Message + NewLine + e.Exception.StackTrace ?? "No stack trace available.", Core.LogLevel.Critical);
				Core.SaveLogFile();
				MessageBox.Show("A critical error has occurred. Details are in ConsoleLog.txt. Please report this and attach the log.", "Very fatal error", MessageBoxButton.OK, MessageBoxImage.Error);
			};

			string processName = Process.GetCurrentProcess().ProcessName;
			Mutex = new Mutex(true, processName, out bool createdNew);

			if (!createdNew)
			{
				// [Ace] Unminimize the already open launcher if you have it minimized.
				foreach (Process p in Process.GetProcessesByName(processName))
				{
					if (IsIconic(p.MainWindowHandle))
					{
						ShowWindow(p.MainWindowHandle, ShowWindowCommands.Restore);
					}
					SetForegroundWindow(p.MainWindowHandle);
					break;
				}

				NoLaunch = true;
				Current.Shutdown();
			}
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			if (!NoLaunch)
			{
				// [Ace] If the launcher is started through cmd or from the start menu by searching for it (it won't be a shortcut), its working directory will either be set to the default startup location (cmd) or system32 (start menu).
				// This forces it to use the physical file's location.
				Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

				Core.Init();
			}
			base.OnStartup(e);
		}
		protected override void OnExit(ExitEventArgs e)
		{
			if (!NoLaunch)
			{
				Core.SaveConfig(fromExit: true);
			}
			base.OnExit(e);
		}
	}
}
